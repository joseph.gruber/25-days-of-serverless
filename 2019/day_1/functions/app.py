from random import choice


def lambda_handler(event, context):
  sides = ['נ (Nun)', 'ג (Gimmel)', 'ה (Hay)', 'ש (Shin)']

  side = choice(sides)

  return {
      "statusCode": 200,
      "body": 'Your dreidel stopped on {}!'.format(side)
  }
